import unittest
from unittest import mock
import guess_number.guess_number as guess_number

class Guesstest(unittest.TestCase):

    @mock.patch('random.randint', return_value=1)
    def test_guess_int_is_1(self, random_randint):
        random_randint.guess_int.return_value = 1
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 1)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=2)
    def test_guess_int_is_2(self, random_randint):
        random_randint.guess_int.return_value = 2
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 2)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=3)
    def test_guess_int_is_3(self, random_randint):
        random_randint.guess_int.return_value = 3
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 3)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=4)
    def test_guess_int_is_4(self, random_randint):
        random_randint.guess_int.return_value = 4
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 4)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=5)
    def test_guess_int_is_5(self, random_randint):
        random_randint.guess_int.return_value = 5
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 5)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=6)
    def test_guess_int_is_6(self, random_randint):
        random_randint.guess_int.return_value = 6
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 6)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=7)
    def test_guess_int_is_7(self, random_randint):
        random_randint.guess_int.return_value = 7
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 7)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=8)
    def test_guess_int_is_8(self, random_randint):
        random_randint.guess_int.return_value = 8
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 8)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=9)
    def test_guess_int_is_9(self, random_randint):
        random_randint.guess_int.return_value = 9
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 9)
        random_randint.assert_called()

    @mock.patch('random.randint', return_value=10)
    def test_guess_int_is_10(self, random_randint):
        random_randint.guess_int.return_value = 10
        result = guess_number.guess_int(1, 10)
        self.assertEqual(result, 10)
        random_randint.assert_called()

    @mock.patch('random.uniform', return_value=1.5)
    def test_guess_float_is_1_5(self, random_uniform):
        random_uniform.guess_float.return_value = 1.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 1.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=2.5)
    def test_guess_float_is_2_5(self, random_uniform):
        random_uniform.guess_float.return_value = 2.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 2.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=3.5)
    def test_guess_float_is_3_5(self, random_uniform):
        random_uniform.guess_float.return_value = 3.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 3.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=4.5)
    def test_guess_float_is_4_5(self, random_uniform):
        random_uniform.guess_float.return_value = 4.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 4.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=5.5)
    def test_guess_float_is_5_5(self, random_uniform):
        random_uniform.guess_float.return_value = 5.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 5.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=6.5)
    def test_guess_float_is_6_5(self, random_uniform):
        random_uniform.guess_float.return_value = 6.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 6.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=7.5)
    def test_guess_float_is_7_5(self, random_uniform):
        random_uniform.guess_float.return_value = 7.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 7.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=8.5)
    def test_guess_float_is_8_5(self, random_uniform):
        random_uniform.guess_float.return_value = 8.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 8.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=9.5)
    def test_guess_float_is_9_5(self, random_uniform):
        random_uniform.guess_float.return_value = 9.5
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 9.5)
        random_uniform.assert_called()

    @mock.patch('random.uniform', return_value=3.8)
    def test_guess_float_is_3_8(self, random_uniform):
        random_uniform.guess_float.return_value = 3.8
        result = guess_number.guess_float(1.0, 10.0)
        self.assertEqual(result, 3.8)
        random_uniform.assert_called()


if __name__ == '__main__':
    unittest.main()
