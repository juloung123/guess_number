"""for random"""
import random


def guess_int(start, stop):
    """for random int"""
    return random.randint(start, stop)


def guess_float(start, stop):
    """for random float"""
    return random.uniform(start, stop)
